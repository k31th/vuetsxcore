import Vue, { VNode } from 'vue';
import app from './App';

declare global {
	namespace JSX {
		interface Element extends VNode { }
		interface ElementClass extends Vue { }
		interface IntrinsicElements {
			[elem: string]: any;
		}
	}
}

new Vue({
	render: h => h(app)
}).$mount('#app');